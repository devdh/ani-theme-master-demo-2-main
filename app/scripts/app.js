'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
    .module('yapp', [
        'ui.router',
        'ngAnimate'
    ])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('/dashboard', '/dashboard/overview');
        $urlRouterProvider.otherwise('/login');

        $stateProvider
            .state('base', {
                abstract: true,
                url: '',
                templateUrl: 'views/base.html'
            })
            .state('login', {
                url: '/login',
                parent: 'base',
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .state('dashboard', {
                url: '/dashboard',
                parent: 'base',
                templateUrl: 'views/dashboard.html',
                controller: 'DashboardCtrl'
            })
            .state('overview', {
                url: '/overview',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/overview.html'
            })
            .state('reports', {
                url: '/reports',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/reports.html'
            })
            .state('closingstock', {
                url: '/closingstock',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/closingstock.html'
            })
            .state('billing', {
                url: '/billing',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/billing.html'
            })
            .state('expenses', {
                url: '/expenses',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/expenses.html'
            })
            .state('order', {
                url: '/order',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/order.html'
            })
            .state('outstanding', {
                url: '/outstanding',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/outstanding.html'
            })
            .state('purchase', {
                url: '/purchase',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/purchase.html'
            })
            .state('sorting', {
                url: '/sorting',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/sorting.html'
            });

    });